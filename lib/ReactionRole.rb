# frozen_string_literal: true

require 'yaml'

# Class responsive of adding role on reaction of a specific message
#
# This class has a reaction data that looks like this:
#   chanel_id:
#     message_id:
#       :expected_message: <String>
#       :reactions:
#         emojiname: :method_to_call_with_event_as_parameter
class ReactionRoleHandle
  # Name of the file containing the reaction data
  FILENAME = 'data/reaction_role.yml'
  # Create a new ReactionRoleHandler
  # @param bot [Discordrb::Commands::CommandBot] the bot that will receive all the commands
  def initialize(bot)
    @bot = bot
    @reaction_data = load_reaction_data
    @must_save = false
    load_messages
    register_command
    register_event
    save_reaction_data
  end

  private

  # @return [Hash]
  def load_reaction_data
    return {} unless File.exist?(FILENAME)

    return YAML.load(File.read(FILENAME))
  rescue StandardError
    puts '[ERROR] Failed to load reaction data properly'
    return {}
  end

  # Save the reaction data
  def save_reaction_data(*)
    return unless @must_save

    File.write(FILENAME, YAML.dump(@reaction_data))
    @must_save = false
  end

  # Function that loads the message.
  # Goals:
  #   - Ensure the message is still there
  #   - Post the message again and save its new ID (if not there)
  #   - Load the reactions for the message to execute all the commands
  # @note This function gets delayed 30 seconds after starting
  def load_messages
    Thread.new do
      puts "  => Starting to load all reaction message"
      @reaction_data.each do |chanel_id, message_data|
        channel = @bot.channel(chanel_id)
        tmp_message_data = {}
        message_data.each do |message_id, data|
          message = channel.load_message(message_id)
          message ? reset_reactions(message, data[:reactions]) : post_message_again(message_data, message_id, channel, tmp_message_data)
        end
        message_data.merge!(tmp_message_data)
      end
      puts "  => Reaction messages loaded!"
    end
  end

  # Function that re-set the reaction on the message
  # @param message [Discordrb::Message]
  # @param reactions [Hash]
  def reset_reactions(message, reactions)
    # message.delete_all_reactions
    reactions.each_key do |reaction_name|
      message.react(reaction_name)
    rescue Exception
      nil
    end
  end

  # Function that posts the reaction message in case it's lost
  # @param message_data [Hash]
  # @param message_id [Integer]
  # @param channel [Discordrb::Channel]
  # @param tmp_message_data [Hash] temporary hash for new messages added after iteration
  def post_message_again(message_data, message_id, channel, tmp_message_data)
    data = message_data[message_id]
    # Post message again
    message = channel.send_message(data[:expected_message])
    return unless message

    # Add all the reactions
    data[:reactions].each_key { |reaction_name| message.react(reaction_name) }

    @must_save = true
    # Delete old message_id
    message_data.delete(message_id)
    # Set new message id
    tmp_message_data[message.id] = data
  rescue StandardError
    p $!
  end

  # Function that register the reaction_add event
  def register_event
    @bot.reaction_add do |reaction|
      chanel_id = reaction.channel.id
      message_id = reaction.message.id
      next unless (method_name = @reaction_data.dig(chanel_id, message_id, :reactions, reaction.emoji.name))

      send(method_name, reaction.user)
      # reaction.message.delete_reaction(reaction.user, reaction.emoji.name)

      # Ensure the bot shut up
      next nil
    end
  end

  # Function that register the reaction commands
  def register_command
    # Create new reaction message
    @bot.command(
      :new_reaction_role,
      help_available: true,
      description: 'Create a new reaction message. `\\\\n` are transformed into new lines, message should be around double quotes.',
      usage: '/new_reaction_role "message shown and expecting reactions"',
      min_args: 1,
      max_args: 1,
      &method(:new_reaction_role)
    )
    # Add reaction to a message
    @bot.command(
      :add_reaction_role,
      help_available: true,
      description: 'Add a reaction that can be clicked to a reaction role message.',
      usage: '/add_reaction_role message_id emoji method_name',
      min_args: 3,
      max_args: 3,
      &method(:add_reaction_role)
    )
  end

  # Function that handle the new reaction message
  # @param event [Discordrb::Commands::CommandEvent]
  # @param message [String]
  def new_reaction_role(event, message)
    user = event.user
    lang = Language.get(user)

    return lang.dig(:generic, :not_allowed) unless user.permission?(:manage_roles)

    message = message.gsub('\\n', "\n")
    new_message = event.send_message(message)
    return nil unless new_message

    # Delete user message
    @bot.failable(event, lang, :generic, :bot_message_not_allowed) { event.message.delete }

    # Save the reaction message
    (@reaction_data[event.channel.id] ||= {})[new_message.id] = { expected_message: message, reactions: {} }
    save_reaction_data(@must_save = true)

    return nil
  end

  # Function that add a reaction role to a message
  # @param event [Discordrb::Commands::CommandEvent]
  # @param message_id [String] ID of the message to parse
  # @param emoji [String] emoji to add as reaction
  # @param method_name [String] method to add
  def add_reaction_role(event, message_id, emoji, method_name)
    user = event.user
    lang = Language.get(user)

    return @bot.error(event, lang, :generic, :not_allowed) unless user.permission?(:manage_roles)

    message_id = message_id.to_i
    channel_id, messages = @reaction_data.find { |_, messages| messages.key?(message_id) }
    return @bot.failure(event, lang, :reaction_role, :message_should_be_reactable) unless channel_id

    method_name = method_name.to_sym
    method_names = methods.grep(/action_/)
    return @bot.failure(event, lang, :reaction_role, :no_method, format_opts: { methods: method_names.join(', ') }) unless method_names.include?(method_name)

    reactions = messages.dig(message_id, :reactions)
    if reactions.key?(emoji)
      reactions[emoji] = method_name
      @bot.success(event, lang, :reaction_role, :reaction_updated)
    else
      @bot.failable(event, lang, :generic, :bot_message_not_allowed) do
        # @type [Discordrb::Channel]
        message_channel = event.server.channels.find { |channel| channel.id == channel_id }
        return @bot.failure(event, lang, :reaction_role, :chanel_not_found) unless message_channel

        message = message_channel.load_message(message_id)
        return @bot.failure(event, lang, :reaction_role, :message_not_found) unless message

        message.react(emoji)
        @bot.success(event, lang, :reaction_role, :reaction_added)
        reactions[emoji] = method_name
      end

    end
    # Save the reaction message
    save_reaction_data(@must_save = true)

    return nil
  end

  # Function that swap roles
  # @param user [Discordrb::Member]
  # @param role_id [Integer] ID of the role
  # @param role_name [String] name of the role
  # @param server_id [Integer] ID of the server for that role
  # @param shadow_role_id [Integer] ID of the role added/removed in background
  def swap_role(user, role_id, role_name, server_id, shadow_role_id = nil)
    return if user.server.id != server_id

    # lang = Language.get(user)
    # Ensure that the member is not loaded from cache (/!\ Can fail if discordrb change)
    user.server.instance_variable_get(:@members).delete(user.id)
    if user.role?(role_id) #(user = @bot.member(user.server, user.id)).role?(role_id)
      return
      puts "  => #{user.name} get rid of #{role_name}"
      user.remove_role(role_id)
      # user.pm(format(lang.dig(:reaction_role, :role_removed), server: user.server.name, role: role_name))
      user.remove_role(shadow_role_id) if shadow_role_id && user.role?(shadow_role_id)
    else
      puts "  => #{user.name} gets #{role_name}"
      user.add_role(role_id)
      # user.pm(format(lang.dig(:reaction_role, :role_added), server: user.server.name, role: role_name))
      user.add_role(shadow_role_id) if shadow_role_id && !user.role?(shadow_role_id)
    end
  rescue StandardError
    puts " => Error while swapping roles"
    puts $!, $!.backtrace
  end

  public

  # Add / remove SDK-FR role to user
  # @param user [Discordrb::Member]
  def action_fr(user)
    swap_role(user, 0x9AC87B5C442003D, 'SDK-FR', 0x1FEF8125C820000, 0x6B69CF3DFC2000C)
  end

  # Add / remove SDK-EN role to user
  # @param user [Discordrb::Member]
  def action_en(user)
    swap_role(user, 0x9AC87861200001F, 'SDK-EN', 0x1FEF8125C820000, 0x6B69CF3DFC2000C)
  end
end
